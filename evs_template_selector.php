<?php
/**
* @author    EVS Webstudio http://www.itmed.su/
* @copyright Copyright (C) EVS Webstudio
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

class plgSystemEvs_Template_Selector extends JPlugin {
	
	public function onAfterInitialise(){
		
		// initialize application
		$input = JFactory::getApplication()->input;
		
		// get session
		$session = JFactory::getSession();
		
		// get param "template"
		$template = $input->getCmd( 'template', '' );
		
		if ( $template !== '' ) {
			$session->set( 'templateChanged', $template );
		}
		
		if ( $session->get( 'templateChanged', '' )!== '' ) {
			$input->set( 'template', $session->get( 'templateChanged', '' ) );
		}
		
	}
	
	public function onContentPrepare($context, &$row, &$params, $page=1) {
		
		preg_match_all('#\[templateselector\]#', $row->text, $matches);
		
		if (count($matches[0])) {
			
			// get session
			$session = JFactory::getSession();
			
			// initialize application
			$app = JFactory::getApplication();
			
			// add css and js files in head
			$doc = JFactory::getDocument();
			
			// add language package
			$language = JFactory::getLanguage();
			$language->load('plg_system_evs_template_selector', JPATH_ADMINISTRATOR, null, true);
			
			// get templates list
			$listSelected = $this->params->get( 'templates', array() );
			// get current template name
			$current_template = $app->getTemplate();
			// get current URI
			$url = JFactory::getURI();
			
			// initialize full template data
			$templates = array();
			
			// initialize database
			$db = JFactory::getDbo();
				
			foreach ($listSelected as $tpl){
				
				$query = $db->getQuery(true);
				$query->select('title');
				$query->from('`#__template_styles`');
				$query->where('`template` = "'.$tpl.'"');
				$query->order('`id` ASC');
					
				$db->setQuery($query);
				$values = $db->loadRow();
				
				$templates[]= array(
						'name' => $tpl,
						'title' => $values[0],
						'thumb' => JURI::base(true). '/templates/'. $tpl. '/template_thumbnail.png'
				);
				
			}
			
			$html = array();
			
			// add previous text
			if ( $this->params->get( 'show_previous_text' ) ) {
				$html[] = '<div>';
				$html[] = $this->get_text( $this->params->get( 'previous_text' ), 'PREVIOUS_TEXT' );
				$html[] = '</div>';
			}
			
			switch( $this->params->get( 'layout' ) ){
				case 2:
					$html[] = $this->layout_list_txt( $templates, $url->current(), $session->get( 'templateChanged', '' ), $current_template, $this->params->get( 'display_thumbnails' ), $this->params->get( 'width' ) );
					break;
				case 3:
					$html[] = $this->layout_list_img( $templates, $session->get( 'templateChanged', '' ) );
					break;
				default:
					$html[] = $this->layout_default( $templates, $url->current(), $session->get( 'templateChanged', '' ), $this->params->get( 'display_thumbnails' ), $this->params->get( 'width' ) );
					if ( $this->params->get( 'display_thumbnails' ) ) 
						$doc->addScriptDeclaration(
						"(function ($) {
							$().ready(function () {
								$('[name=\"template\"]').change(function(){
									$('.evs-template-selector-thumbs img').addClass(\"hidden\");
									$('.'+$(this).val()).removeClass(\"hidden\");
								});
							});
						})(jQuery)"
						); 
					break;
			}
				
			$html = implode('', $html);
				
			$row->text =  str_replace($matches[0][0], $html, $row->text);
			
			$custom_css = $this->params->get( 'custom_css' );
			
			if (empty($custom_css)) {
				$doc->addStyleSheet( JURI::base().'plugins/system/evs_template_selector/tplselector.css' );
			} else {
				$doc->addStyleDeclaration( $custom_css );
			}
			
			
			
		}
		
	}
	
	public function layout_default($templates, $url, $session_var, $display_thumbs, $thumb_width) {
		
		$html = array();
		
		if ($display_thumbs){
			
			$html[] = '<div class="evs-template-selector-thumbs">';
			foreach ($templates as $template){
				$hidden = ( $session_var <> $template['name'] ? ' hidden' : '' );
				$html[] = '<img src="'.$template['thumb'].'" width="'.$thumb_width.'" class="'.$template['name'].$hidden.'" />';
			}
			$html[] = '</div>';
			
		}
		
		$html[] = '<form name="evs_template_selector" action="'.$url.'">';
		$html[] = '<select name="template">';
		foreach ($templates as $template){
			$selected = ( $session_var == $template['name'] ? ' selected' : '' );
			$html[] = '<option value="'.$template['name'].'"'.$selected.'>'.$template['title'].'</option>';
		}
		$html[] = '</select>';
		$html[] = '<button type="submit" class="btn">'.JText::_( 'JSUBMIT' ).'</button>';
		$html[] = '<button type="reset" class="btn">'.JText::_( 'JCANCEL' ).'</button>';
		$html[] = '</form>';
		
		return implode('', $html);
		
	}
	
	public function layout_list_txt($templates, $url, $session_var, $current_template, $display_thumbs, $thumb_width) {
		
		$html = array();
		
		$html[] = '<ul class="evs_template_selector text_list">';
		foreach ($templates as $template){
			$active = ( ($session_var == $template['name']) || ($current_template == $template['name']) ? ' class="active"' : '' );
			$html[] = '<li'.$active.'>';
			if ($display_thumbs){
				$html[] = '<img src="'.$template['thumb'].'" width="'.$thumb_width.'" class="'.$template['name'].'" />';
			}
			$html[] = '<a href="'.$url.'?template='.$template['name'].'"'.$active.' title="'.$template['title'].'">';
			$html[] = '<span>';
			$html[] = $template['title'];
			$html[] = '</span>';
			$html[] = '</a>';
			$html[] = '</li>';
		}
		$html[] = '</ul>';
		
		return implode('', $html);
		
	}
	
	public function layout_list_img($templates, $session_var) {
		
		$html = array();
		$i = 1;
		
		$html[] = '<ul class="evs_template_selector img_list">';
		foreach ($templates as $template){
			$html[] = '<li>';
			$html[] = '<a href="?template='.$template['name'].'" class="btn btn-img evs_tmpsel_img_list_item-'.$i.'" title="'.$template['title'].'"></a>';
			$html[] = '</li>';
			$i++;
		}
		$html[] = '</ul>';
		
		return implode('', $html);
		
	}
	
	public function get_text($text = '', $lang_constant) {
	
		if ( empty($text) ){
			return JText::_( 'PLG_SYSTEM_EVS_TEMPLATE_SELECTOR_'.$lang_constant );
		} else {
			return $text;
		}
	
	}
	
}